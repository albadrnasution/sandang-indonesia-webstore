<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 
	module_load_include('inc', 'popup', 'includes/popup.api');  
 ?>
 
 
 
<?php

//$myThumbnail = "<img src='{$fields[field_image_cache_fid]->content}' alt='' title='{$fields[title]->content}, klik untuk melihat popup.'>";
$myThumbnail = "<span class='berpita'><img src='{$fields[field_image_cache_fid]->content}' alt='' title='{$fields[title]->content}'>";

if ($view->current_display == "page_4")
	$myThumbnail .= "<span class='pita'>minggu ini {$fields[weekcount]->content} / total {$fields[totalcount]->content} kali</span></span>";
else if ($view->current_display == "page_5")
	$myThumbnail .= "<span class='pita'>bulan ini {$fields[monthcount]->content} / total {$fields[totalcount]->content} kali</span></span>";
else if ($view->current_display == "block_2"||$view->current_display == "block_4")
	$myThumbnail .= "<span class='pita'>kunjungan bulan ini {$fields[monthcount]->content} / {$fields[totalcount]->content} kali</span></span>";
else
	$myThumbnail .= "<span class='pita'>Total dikunjungi {$fields[totalcount]->content} kali</span></span>";


$itemPopup = "
<div class='field-title'><h5><a href='{$fields[path]->content}'>{$fields[title]->content}</a></h5>
<div class='field-name'>Oleh {$fields[name]->content} ({$fields[created]->content} lalu)</div>
<div class='field-price'>{$fields[sell_price]->content}</div>
<div class='field-fivestar'>{$fields[value]->content}</div>
<div class='field-uc-thumbnail' style=''>{$fields[field_image_cache_fid_1]->content}</div>
<div class='field-more' style='float:right'><a href='{$fields[path]->content}'>Selengkapnya ...</a></div>
";
?>
<?php
	$popupAtt = array("class"=>"popup-top-content","origin"=>"bottom-right","expand"=>"top-left",
		"link"=>$fields["path"]->content,"activate"=>"hover","close"=>true,"width"=>"300");

	print popup_element($myThumbnail, $itemPopup, $popupAtt);
?>
	




