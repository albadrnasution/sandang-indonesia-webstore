<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 
	module_load_include('inc', 'popup', 'includes/popup.api');  
 ?>
 
 
 
<?php

$myThumbnail = "<img src='{$fields[field_image_cache_fid]->content}' alt='' title='{$fields[title]->content}' />";


$itemPopup = "
<div class='field-title'><h5><a href='{$fields[path]->content}'>{$fields[title]->content}</a></h5>
<div class='field-name'>Oleh {$fields[name]->content}</div>
<div class='field-date'>({$fields[created]->content})</div>
<div class='field-price'>{$fields[sell_price]->content}</div>
<div class='field-fivestar'>{$fields[value]->content}</div>
<div class='field-uc-thumbnail' style=''>{$fields[field_image_cache_fid_1]->content}</div>";
?>
<?php
	$popupAtt = array("class"=>"popup-top-content","origin"=>"bottom-right","expand"=>"top-left",
		"link"=>$fields["path"]->content,"activate"=>"hover","close"=>true,"width"=>"180");
	print popup_element($myThumbnail, $itemPopup, $popupAtt);
?>
	




