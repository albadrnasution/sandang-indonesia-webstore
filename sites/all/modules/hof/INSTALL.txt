$Id: INSTALL.txt,v 1.3 2007/03/23 01:37:01 syscrusher Exp $

Installation Instructions for "hof.module"


1. Copy the files in this directory to directory "modules/hof/"
   under your Drupal root directory. There is no special SQL table for this
   module.

2. Login to Drupal as the administrative user, and enable the Hall of Fame module.

3. Go to admin/settings/hof and enable the desired reports, and set other features
   as you prefer.

4. Set appropriate permissions for who can access the module's various display
   pages. NOTE: The module offers permissions based on what reports are currently
   enabled, so you really need to do step 3 before you set the permissions.

   Note also that by default ONLY the system administrator can access HOF.

5. The main display page for the hof module is http://www.example.com/hof (if your
   Drupal installation is not in a subdirectory). This will not automatically
   appear in any menu, so you may want to add it to your custom menus (such as
   the primary or secondary menu in your theme).


Upgrade Instructions for "hof.module"

To upgrade, simply copy the new hof.module file over the old one in your Drupal
directory structure. There is currently no need for any special upgrade script.

You may want to go to admin/settings/hof to see if there are new features you
might wish to configure, but the module has reasonable defaults for all
settings, so this step is optional.
