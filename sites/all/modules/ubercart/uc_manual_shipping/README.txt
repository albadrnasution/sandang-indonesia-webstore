# README file for uc_manual_shipping
# Copyright (C) 2011 Stewart Adam <s.adam at diffingo.com>
# License: GNU GPLv2+


What does uc_manual_shipping do?
-------------------------
* Adds new 'shipping' order email template that is used to inform customers when
  their order's shipping quote is ready.
* Adds an empty ($0.00) shipping line item to all new products so that
  administrators can fill in a correct value later.


Installation
------------
Simply copy the uc_manual_shipping folder into your sites/all/modules folder and
then browse to admin/build/module to enable it.


Configuration
-------------
Browse to admin/store/settings/manual_shipping and configure the manual method
name.


Caveats and known bugs
----------------------
* Does not implement hook_shipping_method(), so this module does not present
  itself as an actual shipping method to Ubercart.
