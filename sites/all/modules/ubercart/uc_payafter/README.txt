# README file for uc_payafter
# Copyright (C) 2011 Stewart Adam <s.adam at diffingo.com>
# License: GNU GPLv2+

If you are looking to extend or modify uc_payafter, any comments or details you
need to know are included in the source code itself as well as the DEVELOPERS
file. However, I would like to re-iterate this important warning for all users
and developers of uc_payafter:

*******************************************************************************
Because I wanted this module to adhere to Drupal module development standards
as much as possible, I opted to avoid hacking core at all costs and thus there
is a significant amount of code overlap with uc_cart. Seeing as such, do keep
an eye out for security bugs in uc_cart and apply the same fix here if
nescessary.
*******************************************************************************


What does uc_payafter do?
-------------------------
* Enables URLs in the form of cart/checkout/pay/$ORDER_ID so customers can
  complete payments on orders after the checkout process.
* Creates payment checkout pane settings as well as payment checkout messages
  configurable at at admin/store/settings/checkout/edit
* Adds new conditional actions for payment checkout completion, and by default
  triggers the 'customer' and 'admin' template emails when payment checkout
  completes (see configuration below for more information).
* Adds new 'request_admin' and 'request_customer' order email templates.


Installation
------------
Simply copy the uc_payafter folder into your sites/all/modules folder and then
browse to admin/build/module to enable it.


Configuration
-------------
  (Re-)Configure the checkout & payment panes and messages
  --------------------------------------------------------
  Browse to the store checkout settings at admin/store/settings/checkout and
  configure your store's checkout & payment panes and messages.
  
  Modifying the store's default update actions
  --------------------------------------------
  By default, the 'customer' and 'admin' templates are used for the initial
  emails sent out once checkout has completed. uc_payafter defines two new
  conditional actions that are trigged when payment checkout is completed, also
  using the 'customer' and 'admin' templates.
  
  uc_payafter also, for your convinience, defines two new templates named
  'request_customer' and 'request_admin' that you can use as you see fit. In the
  recommended setup, 'request_customer' & 'request_admin' are used when the
  "original" checkout process is completed (you will need to configure this),
  and 'customer' & 'admin' when payment is completed (default settings).
  
  To configure the store's conditional actions:
  1. Browse to admin/store/ca
  2. Select one of the actions, for example "E-mail customer checkout
     notification" and click "Edit"
  3. In the "Actions" tab, expand the default action and select the appropriate
     invoice template under "Invoice template".
  4. Customize the rest of the form if needed, then save.
  
  Generating your custom customer/admin templates
  -------------------------------------------------
  Although uc_payafter defines the two template names 'request_customer' and
  'request_admin', some initial setup is required before these templates can be
  used correctly by the Ubercart invoice template system.
  
  1. Copy the file "uc_order.tpl.php" from the Ubercart order templates folder
     in sites/all/modules/ubercart/uc_order/templates into your active theme's
     folder (most probably sites/all/themes/<active theme>). Do the same for
     "uc_order-customer.tpl.php", except name it 
     "uc_order-request_customer.tpl.php" after placing it in your active theme's
     folder. Similarly, copy "uc_order-admin.tpl.php" from the order templates
     folder to your theme folder as "uc_order-request_admin.tpl.php".
  2. Clear all caches by browsing to admin/settings/performance and clicking the
     "Clear All Caches" button.
  3. Edit the new template files and customize them to your needs.



Caveats and known bugs
----------------------
* At the moment, there is no support for an alternate "checkout completion" page
  after payment has been completed.

* This module most probably breaks step-by-step checkouts. If you want to fix
  it, you will need to re-implement the _uc_cart_checkout_{next,prev}_pane
  functions so that they pay attention to the uc_payafter_pane* variables
  instead of the default Ubercart ones.

* If a user fills out the payment information, as soon as the user clicks
  "Review" the order information is updated. This means that the user can enter
  a bogus name and address for example, and the information will be saved in the
  order until the user fills out the payment form again. I'm not sure there if
  there is a good way to work around this. Keep in mind that because of this
  problem you should *ALWAYS* have some form of access control, be it hashtags
  in the URL or user login verification (included in the module).

* Order comments always get a single dash, I haven't investiaged that yet.

