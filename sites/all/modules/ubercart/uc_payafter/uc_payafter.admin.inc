<?php
# Copyright (C) 2011 Stewart Adam <s.adam at diffingo.com>

# uc_payafter is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# uc_payafter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with uc_payafter; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# This module uses (at times, rather large) code segments copied from the files
# contained in Ubercart's source code distributions. At the time of writing, the
# Ubercart contributors' copyrights are not listed in the header of module
# files, so I would like to refer to the CREDITS.txt file included in the latest
# release as of writing (6.x-2.4) to give the Ubercart authors credit:
# http://drupalcode.org/project/ubercart.git/blob_plain/bf4dc43e99329a674aa4c4625ec6f87b96a43c4e:/CREDITS.txt

/**
 * Mimicks uc_cart_checkout_panes_form() in uc_cart.admin.inc.
 *
 * Settings for panes on the payment checkout page.
 */
function uc_payafter_checkout_panes_form() {
  $panes = _payafter_checkout_pane_list();

  $form['panes'] = array(
    '#theme' => 'uc_pane_sort_table',
    '#pane_prefix' => 'uc_payafter_pane',
    '#summary callback' => '_uc_cart_panes_summarize',
    '#summary arguments' => array($panes),
  );
  foreach ($panes as $pane) {
    $form['panes'][$pane['id']]['uc_payafter_pane_'. $pane['id'] .'_enabled'] = array(
      '#type' => 'checkbox',
      '#default_value' => $pane['enabled'],
    );
    $form['panes'][$pane['id']]['title'] = array(
      '#value' => $pane['title'],
    );
    $form['panes'][$pane['id']]['uc_payafter_pane_'. $pane['id'] .'_weight'] = array(
      '#type' => 'weight',
      '#default_value' => $pane['weight'],
    );
  }

  return system_settings_form($form);
}

/**
 * Mimicks uc_cart_checkout_messages_form() in uc_cart.admin.inc.
 *
 * Settings for help messages displayed on the checkout page.
 */
function uc_payafter_checkout_messages_form() {
  $form['uc_payafter_checkout_instructions'] = array(
    '#type' => 'textarea',
    '#title' => t('Payment instructions'),
    '#description' => t('Provide instructions for customers at the top of the payment screen.'),
    '#summary' => variable_get('uc_payafter_checkout_instructions', '') ? t('Payment instructions are set.') : t('Payment instructions are not set.'),
    '#default_value' => variable_get('uc_payafter_checkout_instructions', ''),
    '#rows' => 3,
  );
  $form['uc_payafter_checkout_instructions_format'] = filter_form(variable_get('uc_payafter_checkout_instructions_format', FILTER_FORMAT_DEFAULT), NULL, array('uc_payafter_checkout_instructions_format'));

  $form['uc_payafter_checkout_review_instructions'] = array(
    '#type' => 'textarea',
    '#title' => t('Payment review instructions'),
    '#description' => t('Provide instructions for customers at the top of the Payment review screen.'),
    '#summary' => variable_get('uc_payafter_checkout_review_instructions', uc_get_message('review_instructions'))
               ? t('Payment review instructions are set.')
               : t('Payment review instructions are not set.'),
    '#default_value' => variable_get('uc_payafter_checkout_review_instructions', uc_get_message('review_instructions')),
    '#rows' => 3,
  );
  $form['uc_payafter_checkout_review_instructions_format'] = filter_form(variable_get('uc_payafter_checkout_review_instructions_format', FILTER_FORMAT_DEFAULT), NULL, array('uc_payafter_checkout_review_instructions_format'));

  $form['uc_payafter_msg_order_submit'] = array(
    '#type' => 'textarea',
    '#title' => t('Payment completion message header'),
    '#description' => t('Header for message displayed after a user completes a payment. <a href="!url">Uses order and global tokens</a>.', array('!url' => url('admin/store/help/tokens'))),
    '#summary' => variable_get('uc_payafter_msg_order_submit', uc_get_message('completion_message'))
               ? t('Completion message is set.')
               : t('Completion message is not set.'),
    '#default_value' => variable_get('uc_payafter_msg_order_submit', uc_get_message('completion_message')),
    '#rows' => 3,
  );
  $form['uc_payafter_msg_order_submit_format'] = filter_form(variable_get('uc_payafter_msg_order_submit_format', FILTER_FORMAT_DEFAULT), NULL, array('uc_msg_order_submit_format'));

  $form['payafter_checkout_messages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment completion message body'),
    '#description' => t("The appropriate body message will automatically be selected."),
    '#summary callback' => 'summarize_form',
    '#collapsible' => FALSE,
  );
  $form['payafter_checkout_messages']['uc_payafter_msg_order_logged_in'] = array(
    '#type' => 'textarea',
    '#title' => t('Payment completion for logged-in users'),
    '#description' => t('Message displayed upon payment completion for a user who has logged in. <a href="!url">Uses order and global tokens</a>.', array('!url' => url('admin/store/help/tokens'))),
    '#summary' => variable_get('uc_payafter_msg_order_logged_in', uc_get_message('completion_logged_in'))
               ? t('Completion text for logged in users is set.')
               : t('Completion text for logged in users is not set.'),
    '#default_value' => variable_get('uc_payafter_msg_order_logged_in', uc_get_message('completion_logged_in')),
    '#rows' => 3,
  );
  $form['payafter_checkout_messages']['uc_payafter_msg_order_logged_in_format'] = filter_form(variable_get('uc_payafter_msg_order_logged_in_format', FILTER_FORMAT_DEFAULT), NULL, array('uc_payafter_msg_order_logged_in_format'));

  $form['payafter_checkout_messages']['uc_payafter_msg_order_existing_user'] = array(
    '#type' => 'textarea',
    '#title' => t('Payment completion for existing users'),
    '#description' => t("Message displayed upon payment completion for a user who has an account but wasn't logged in. <a href=\"!url\">Uses order and global tokens</a>.", array('!url' => url('admin/store/help/tokens'))),
    '#summary' => variable_get('uc_payafter_msg_order_existing_user', uc_get_message('completion_existing_user'))
               ? t('Completion text for users who aren\'t logged in is set.')
               : t('Completion text for users who aren\'t logged in is not set.'),

    '#default_value' => variable_get('uc_payafter_msg_order_existing_user', uc_get_message('completion_existing_user')),
    '#rows' => 3,
  );
  $form['payafter_checkout_messages']['uc_payafter_msg_order_existing_user_format'] = filter_form(variable_get('uc_payafter_msg_order_existing_user_format', FILTER_FORMAT_DEFAULT), NULL, array('uc_payafter_msg_order_existing_user_format'));

  $form['uc_payafter_msg_continue_shopping'] = array(
    '#type' => 'textarea',
    '#title' => t('Continue shopping message'),
    '#description' => t('Message displayed upon payment completion to direct customers to another part of your site. <a href="!url">Uses order and global tokens</a>.', array('!url' => url('admin/store/help/tokens'))),
    '#summary' => variable_get('uc_payafter_msg_continue_shopping', uc_get_message('continue_shopping'))
               ? t('Continue shopping text is set.')
               : t('Continue shopping text is not set.'),
    '#default_value' => variable_get('uc_payafter_msg_continue_shopping', uc_get_message('continue_shopping')),
    '#rows' => 3,
  );
  $form['uc_payafter_msg_continue_shopping_format'] = filter_form(variable_get('uc_payafter_msg_continue_shopping_format', FILTER_FORMAT_DEFAULT), NULL, array('uc_payafter_msg_continue_shopping_format'));

  return system_settings_form($form);
}
