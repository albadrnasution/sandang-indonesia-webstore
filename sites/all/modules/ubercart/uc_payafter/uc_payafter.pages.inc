<?php
# Copyright (C) 2011 Stewart Adam <s.adam at diffingo.com>

# uc_payafter is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# uc_payafter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with uc_payafter; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# This module uses (at times, rather large) code segments copied from the files
# contained in Ubercart's source code distributions. At the time of writing, the
# Ubercart contributors' copyrights are not listed in the header of module
# files, so I would like to refer to the CREDITS.txt file included in the latest
# release as of writing (6.x-2.4) to give the Ubercart authors credit:
# http://drupalcode.org/project/ubercart.git/blob_plain/bf4dc43e99329a674aa4c4625ec6f87b96a43c4e:/CREDITS.txt

/**
 * Mimicks uc_cart_checkout() in uc_cart/uc_cart.pages.inc.
 *
 * Displays payment entry form by mimicking regular Ubercart checkout
 * conditions, but forcibly displaying only the payment pane (even if it is
 * disabled in the Ubercart settings).
 */
function uc_payafter_checkout($hashtag) {
  global $user;
  
  $order = uc_order_load(intval($hashtag));
  if ($order === FALSE) {
    drupal_goto();
  }
  $_SESSION['cart_order'] = intval($hashtag);
  
  if (uc_order_status_data($order->order_status, 'state') != 'post_checkout') {
    drupal_set_message(t('Our records indicate that order #@id has already been processed. If you believe this is an error, please contact us via email or telephone.', array('@id' => $hashtag)), 'error');
    drupal_goto();
  }
  
  // Send anonymous users to login page
  if ($user->uid <= 0) {
    drupal_set_message(t('You must login before you can proceed to payment.'));
    $_SESSION['payafter-checkout-redirect'] = TRUE;
    drupal_goto('user');
  }
  else {
    unset($_SESSION['payafter-checkout-redirect']);
  }
  
  // Verify user performing payment is the user who created the order
  if ($user->uid !== $order->uid) {
    drupal_set_message(t('Order #@id was not created by your user!', array('@id' => $_SESSION['cart_order'])), 'error');
    drupal_goto();
  }
  
  // Use the order's stored line items instead of using the hook_line_item()
  // callbacks.
  foreach ($order->line_items as $line_item) {
    if ($line_item['type'] == 'tax') { continue; }
    $js = sprintf('$(document).ready( function() { set_line_item("%s", "%s", %f, %d) });', $line_item['type'], $line_item['title'], $line_item['amount'], $line_item['weight']);
    drupal_add_js($js, 'inline');
  }
  
  drupal_add_js(drupal_get_path('module', 'uc_cart') .'/uc_cart.js');
  $output = drupal_get_form('uc_payafter_checkout_form');
  
  return $output;
}

/**
 * Mimicks uc_cart_checkout_form() in uc_cart/uc_cart.pages.inc.
 *
 * Note that we must re-use the same session variables as used during regular
 * checkout in order to "trick" the system. For example, we must use
 * $_SESSION['cart_order'] instead of a more appropriate name here
 * because functions like uc_payment_get_details() require it to be named such.
 */
function uc_payafter_checkout_form() {
  global $user;
  $order = uc_order_load($_SESSION['cart_order']);
  $url = 'cart/checkout/pay/'.$_SESSION['cart_order'];
  
  // Confirm the cancellation when user clicks the 'Cancel' button.
  if ($_POST['op'] == t('Cancel')) {
    drupal_goto($url.'/cancel');
  }
  
  // If the cart isn't shippable, remove panes with shippable == TRUE.
  if (!uc_cart_is_shippable() && variable_get('uc_cart_delivery_not_shippable', TRUE)) {
    $panes = uc_cart_filter_checkout_panes($panes, array('shippable' => TRUE));
  }

  $form['panes'] = array('#tree' => TRUE);
  $panes = _payafter_checkout_pane_list();
  
  foreach ($panes as $pane) {
    if (variable_get('uc_payafter_pane_'. $pane['id'] .'_enabled', $pane['enabled'])) {
      // _uc_cart_checkout_{prev,next}_pane are checking the wrong variables,
      // so I am *sure* that this won't work. If you want to get this working,
      // you would need to re-implement these following two functions here
      // and check against the uc_payafter_pane*_enabled variables instead.
      $pane['prev'] = _uc_cart_checkout_prev_pane($panes, $pane['id']);
      $pane['next'] = _uc_cart_checkout_next_pane($panes, $pane['id']);

      if (is_null($pane['collapsed'])) {
        $collapsed = ($pane['prev'] === FALSE || empty($displayed[$pane['prev']])) ? FALSE : TRUE;
      }
      if (isset($_SESSION['expanded_panes'])) {
        if (is_array($_SESSION['expanded_panes']) &&
            in_array($pane['id'], $_SESSION['expanded_panes'])) {
          $collapsed = FALSE;
        }
      }
      
      $return = $pane['callback']('view', $order, NULL);
      
      switch ($pane['id']) {
        case 'customer':
          // Modify the customer information "edit" button so that we redirect to
          // the appropriate page after editing their user information.
          $replaced = str_replace("=cart/checkout", "=cart/checkout/pay/".$_SESSION['cart_order'], $return['contents']['email_text']['#value']);
          $return['contents']['email_text']['#value'] = $replaced;
          break;
        
        case 'delivery':
          if (_payafter_checkout_pane_data('billing', 'weight') < _payafter_checkout_pane_data('delivery', 'weight') &&
              _payafter_checkout_pane_data('billing', 'enabled')) {
            // Force the display of the copy checkbox, regardless of cart contents.
            // See uc_checkout_pane_delivery in uc_cart/uc_cart_checkout_pane.inc
            $return['contents']['copy_address'] = array(
              '#type' => 'checkbox',
              '#title' => t('My delivery information is the same as my billing information.'),
              '#attributes' => array('onclick' => "uc_cart_copy_address(this.checked, 'billing', 'delivery');"),
            );
          }
          break;
        
        case 'billing':
          // Force the display of the copy checkbox, regardless of cart contents.
          // See uc_checkout_pane_delivery in uc_cart/uc_cart_checkout_pane.inc
          if (_payafter_checkout_pane_data('delivery', 'weight') < _payafter_checkout_pane_data('billing', 'weight') &&
              _payafter_checkout_pane_data('delivery', 'enabled')) {
            $return['contents']['copy_address'] = array(
              '#type' => 'checkbox',
              '#title' => t('My billing information is the same as my delivery information.'),
              '#attributes' => array('onclick' => "uc_cart_copy_address(this.checked, 'delivery', 'billing');"),
            );
          }
          break;
      }
  
      // Add the pane if any display data is returned from the callback.
      if (is_array($return) && (!empty($return['description']) || !empty($return['contents']))) {
        // Create the fieldset for the pane.
        $form['panes'][$pane['id']] = array(
          '#type' => 'fieldset',
          '#title' => $pane['title'],
          '#description' => !empty($return['description']) ? $return['description'] : NULL,
          '#collapsible' => $pane['collapsible'],
          '#collapsed' => FALSE,
          '#attributes' => array('id' => $pane['id'] .'-pane'),
          '#theme' => $return['theme'],
        );
  
        // Add the contents of the fieldset if any were returned.
        if (!empty($return['contents'])) {
          $form['panes'][$pane['id']] = array_merge($form['panes'][$pane['id']], $return['contents']);
        }
  
        // Log that this pane was actually displayed.
        $displayed[$pane['id']] = TRUE;
      }
    }
  }
  unset($_SESSION['expanded_panes']);

  $form['cart_contents'] = array(
    '#type' => 'hidden',
    '#value' => serialize($order->products),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => FALSE,
  );
  $form['continue'] = array(
    '#type' => 'submit',
    '#value' => t('Review order'),
  );
  
  return $form;
}

/**
 * Mimicks uc_cart_checkout_form_validate() in uc_cart/uc_cart.pages.inc.
 */
function uc_payafter_checkout_form_validate($form, &$form_state) {
  if (empty($_SESSION['cart_order'])) {
    drupal_goto();
  }
  
  $order = uc_order_load($_SESSION['cart_order']);
  
  $order->order_total = uc_order_get_total($order, TRUE);
  
  // Validate/process the cart panes.  A FALSE value results in failed checkout.
  $_SESSION['checkout_valid'] = TRUE;
  
  foreach (element_children($form_state['values']['panes']) as $pane_id) {
    $func = _payafter_checkout_pane_data($pane_id, 'callback');
    $isvalid = $func('process', $order, $form_state['values']['panes'][$pane_id]);
    if ($isvalid === FALSE) {
      $_SESSION['expanded_panes'][] = $pane_id;
      $_SESSION['checkout_valid'] = FALSE;
    }
  }
  
  $order->line_items = uc_order_load_line_items($order, TRUE);
  
  uc_order_save($order);
}

/**
 * Mimicks uc_cart_checkout_form_submit() in uc_cart/uc_cart.pages.inc.
 */
function uc_payafter_checkout_form_submit($form, &$form_state) {
  if ($_SESSION['checkout_valid'] === FALSE) {
    $url = 'cart/checkout/pay/'.$_SESSION['cart_order'];
  }
  else {
    $url = 'cart/checkout/pay/review';
    $_SESSION['do_review'] = TRUE;
  }

  unset($_SESSION['checkout_valid']);
  $form_state['redirect'] = $url;
}

/**
 * Mimicks theme_uc_cart_checkout_form() in uc_cart/uc_cart.pages.inc.
 */
function theme_uc_payafter_checkout_form($form) {
  drupal_add_css(drupal_get_path('module', 'uc_cart') .'/uc_cart.css');

  $output = '<div id="checkout-instructions">'. check_markup(variable_get('uc_payafter_checkout_instructions', ''), variable_get('uc_payafter_checkout_instructions_format', FILTER_FORMAT_DEFAULT), FALSE) .'</div>';

  foreach (element_children($form['panes']) as $pane_id) {
    if (function_exists(($func = _payafter_checkout_pane_data($pane_id, 'callback')))) {
      $result = $func('theme', $form['panes'][$pane_id], NULL);
      if (!empty($result)) {
        $output .= $result;
        $form['panes'][$pane_id] = array();
      }
      else {
        $output .= drupal_render($form['panes'][$pane_id]);
      }
    }
    else {
      $output .= drupal_render($form['panes'][$pane_id]);
    }
  }

  $output .= '<div id="checkout-form-bottom">'. drupal_render($form) .'</div>';

  return $output;
}


/**
 * Confirmation form to cancel an order.
 */
function uc_payafter_cancel_confirm_form($form_state, $order_id) {
  $url = 'cart/checkout/pay/'.$order_id;
  if (!uc_referer_check(array($url, $url.'/cancel'))) {
    drupal_goto($url);
  }
  $order = uc_order_load($order_id);
  
  if ($order === FALSE) {
    drupal_goto();
  }
  
  if (uc_order_status_data($order->order_status, 'state') == 'canceled') {
    drupal_set_message(t('Our records indicate that order #@id has already been cancelled. If you believe this is an error, please contact us via email or telephone.', array('@id' => $order_id)), 'error');
    drupal_goto();
  }
  
  $form['order_id'] = array(
    '#type' => 'value',
    '#value' => $order_id,
  );
  
  $text = t('Are you sure you want to cancel order @order_id?', array('@order_id' => $order_id));
  
  return confirm_form($form, $text, $url, $text, t('Cancel my order'), t('Back'));
}

/**
 * Cancel an order.
 */
function uc_payafter_cancel_confirm_form_submit($form, &$form_state) {
  // Add a comment and cancel the order.
  uc_order_comment_save($form_state['values']['order_id'], 0, t('Customer cancelled this order from the payment form.'));
  $returned = uc_order_update_status($form_state['values']['order_id'], uc_order_state_default('canceled'));
  
  // Let the user know and redirect them to the home page.
  drupal_set_message(t('Your order request has been cancelled.'));
  $form_state['redirect'] = '';
}


/**
 * Mimicks uc_cart_checkout_review() in uc_cart/uc_cart.pages.inc.
 * 
 * Allow a customer to review their order before finally submitting it.
 */
function uc_payafter_checkout_review() {
  drupal_add_js(drupal_get_path('module', 'uc_cart') .'/uc_cart.js');
  $form = drupal_get_form('uc_payafter_checkout_review_form');
  
  $url = 'cart/checkout/pay/'.$_SESSION['cart_order'];
  if ($_SESSION['do_review'] !== TRUE && !uc_referer_check($url)) {
    drupal_goto($url);
  }
  unset($_SESSION['do_review']);

  $order = uc_order_load($_SESSION['cart_order']);

  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'post_checkout') {
    drupal_set_message(t('Our records indicate that order #@id has already been processed. If you believe this is an error, please contact us via email or telephone.', array('@id' => $_SESSION['cart_order'])), 'error');
    unset($_SESSION['cart_order']);
    drupal_goto($url);
  }

  $panes = _payafter_checkout_pane_list();
  
  // If the order isn't shippable, bypass panes with shippable == TRUE.
  if (!uc_order_is_shippable($order) && variable_get('uc_cart_delivery_not_shippable', TRUE)) {
    $panes = uc_cart_filter_checkout_panes($panes, array('shippable' => TRUE));
  }

  foreach ($panes as $pane) {
    if (variable_get('uc_payafter_pane_'. $pane['id'] .'_enabled', TRUE)) {
      $func = $pane['callback'];
      if (function_exists($func)) {
        $return = $func('review', $order, NULL);
        if (!is_null($return)) {
          $data[$pane['title']] = $return;
        }
      }
    }
  }

  $output = theme('uc_payafter_checkout_review', $data, $form);

  return $output;
}

/**
 * Mimicks uc_cart_checkout_review_form() in uc_cart/uc_cart.pages.inc.
 *
 * Give customers the option to finish payment or go revise their information.
 */
function uc_payafter_checkout_review_form() {
  // Set the session variable to pass the redirect check on the pageload.
  if ($_POST['op'] == t('Back')) {
    $_SESSION['do_review'] = TRUE;
  }

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => array('uc_payafter_checkout_review_form_back'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit order'),
  );

  return $form;
}

/**
 * Mimicks uc_cart_checkout_review_form() in uc_cart/uc_cart.pages.inc.
 * 
 * Returns the customer to the first payment page to edit their information.
 */
function uc_payafter_checkout_review_form_back($form, &$form_state) {
  unset($_SESSION['do_review']);
  $form_state['redirect'] = 'cart/checkout/pay/'.$_SESSION['cart_order'];
}

/**
 * Mimicks uc_cart_checkout_review_form_submit() in uc_cart/uc_cart.pages.inc.
 *
 * Final checks to make sure the payment can be completed.
 */
function uc_payafter_checkout_review_form_submit($form, &$form_state) {
  // Invoke hook_order($op = 'submit') to test to make sure the order can
  // be completed... used for auto payment in uc_credit.module.
  $order = uc_order_load($_SESSION['cart_order']);
  $error = FALSE;

  // Invoke it on a per-module basis instead of all at once.
  foreach (module_list() as $module) {
    $function = $module .'_order';
    if (function_exists($function)) {
      // $order must be passed by reference.
      $result = $function('submit', $order, NULL);

      $msg_type = 'status';
      if ($result[0]['pass'] === FALSE) {
        $error = TRUE;
        $msg_type = 'error';
      }
      if (!empty($result[0]['message'])) {
        drupal_set_message($result[0]['message'], $msg_type);
      }

      // Stop invoking the hooks if there was an error.
      if ($error) {
        break;
      }
    }
  }

  if ($error) {
    $_SESSION['do_review'] = TRUE;
    $form_state['redirect'] = 'cart/checkout/pay/review';
  }
  else {
    $_SESSION['do_complete'] = TRUE;
    $form_state['redirect'] = 'cart/checkout/pay/complete';
  }
}

/**
 * Mimicks theme_uc_cart_checkout_review() in uc_cart/uc_cart.pages.inc.
 *
 * Theme the checkout review order page.
 */
function theme_uc_payafter_checkout_review($panes, $form) {
  drupal_add_css(drupal_get_path('module', 'uc_cart') .'/uc_cart.css');

  $output = check_markup(variable_get('uc_payafter_checkout_review_instructions', uc_get_message('review_instructions')), variable_get('uc_payafter_checkout_review_instructions_format', FILTER_FORMAT_DEFAULT), FALSE)
           .'<table class="order-review-table">';

  foreach ($panes as $title => $data) {
    $output .= '<tr class="pane-title-row"><td colspan="2">'. $title
              .'</td></tr>';
    if (is_array($data)) {
      foreach ($data as $row) {
        if (is_array($row)) {
          if (isset($row['border'])) {
            $border = ' class="row-border-'. $row['border'] .'"';
          }
          else {
            $border = '';
          }
          $output .= '<tr valign="top"'. $border .'><td class="title-col" '
                    .'nowrap>'. $row['title'] .':</td><td class="data-col">'
                   . $row['data'] .'</td></tr>';
        }
        else {
          $output .= '<tr valign="top"><td colspan="2">'. $row .'</td></tr>';
        }
      }
    }
    else {
      $output .= '<tr valign="top"><td colspan="2">'. $data .'</td></tr>';
    }
  }

  $output .= '<tr class="review-button-row"><td colspan="2">'. $form
            .'</td></tr></table>';

  return $output;
}

/**
 * Mimicks uc_cart_checkout_complete() in uc_cart/uc_cart.pages.inc.
 *
 * Completes finishes payment and completes the order.
 */
function uc_payafter_checkout_complete() {
  $url = 'cart/checkout/pay/'.$_SESSION['cart_order'];
  if (!$_SESSION['do_complete']) {
    drupal_goto($url);
  }

  $order = uc_order_load(intval($_SESSION['cart_order']));

  if (empty($order)) {
    // Display messages to customers and the administrator if the order was lost.
    drupal_set_message(t("We're sorry.  An error occurred while processing your order that prevents us from completing it at this time. Please contact us and we will resolve the issue as soon as possible."), 'error');
    watchdog('uc_payafter', 'An empty order made it to payment checkout! Cart order ID: @cart_order', array('@cart_order' => $_SESSION['cart_order']), WATCHDOG_ERROR);
    drupal_goto($url); 
  }

  $output = uc_payafter_complete_sale($order);

  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order payment processed through website.'), 'admin');

  /*
  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }
  */

  return $output;
}

/**
 * Mimicks theme_uc_cart_complete_sale() in uc_cart/uc_cart.module.
 *
 * Theme the payment completion page.
 */
function theme_uc_payafter_complete_sale($message) {
  return $message;
}