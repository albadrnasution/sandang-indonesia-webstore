<?php

/**
 * @file
 * Theme implementation to display a list of visitors to a profile.
 *
 * Available variables:
 *
 * - $visitors: List of visitors themed with theme_item_list.
 * - $visitors_list: Unthemed array of visits.
 */
?>

<div class="profile-visitors">
  <?php print $visitors ?>
</div>